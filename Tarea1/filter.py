from tkinter import *
import tkinter.filedialog
from tkinter import messagebox
import cv2
import sys
import time
import imutils
import numpy as np
from matplotlib import pyplot as plt

#Código en caso de querer seleccionar la imagen de archivos
#messagebox.showinfo("Eligir", "Seleccione la imagen a utilizar")
#file = tkinter.filedialog.askopenfilename()

img = cv2.imread( "./mandarina1.jpeg" ) #Es posible utilizar las demás imagenes solo con cambiar la dirección
bandera = True

def main(): #Menú principal
    print("Elija la opcion a realizar")
    print("1. Tranformar")
    print("2. Filtros")
    print( "3. Salir" )
    opcion1 = input("Indique la opcion a realizar: ")
    if (opcion1 == "1"):
        change()
    elif (opcion1 == "2"):
        filters()
    elif (opcion1 == "3"):
        sys.exit(0)
    else:
        main()

#Función para obtener el rgb de un pixel solicitando al usuario las coordenadas x, y

def rgbImage():
    x = input( "Ingrese el valor de x: " )
    y = input("Ingrese el valor de y: ")
    (B, G, R) = img[int(x), int(y)]
    print( "R={}, G={}, B={}".format( R, G, B ) )
    time.sleep(3)
    print("\n")
    again()

#Función para menú de volver
def again():
    print("1. Volver al menú")
    print("2. Salir")
    option = input("Elija una opción: ")
    if (option == "1"):
        global bandera
        if (bandera):
            change()
        else:
            filters()
    elif (option == "2"):
        sys.exit(0)
    else:
        again()

#Función para ajustar el tamaño de una imagen
def resize():
    x = input( "Ingrese el ancho: " )
    y = input( "Ingrese la altura: " )
    resized = cv2.resize( img, (int(x), int(y)) )
    show(resized, "Ajustar")


#Función para redimensionar una imagen
def resize2():
    x = input( "Ingrese el ancho: " )
    y = input( "Ingrese la altura: " )
    resized = imutils.resize( img, width=int(x), height=int(y))
    cv2.imshow( "Imutils Resize", resized )
    cv2.imshow("Original", img)
    again()

#Función para rotar una imagen
def rotate():
    x = input( "Ingrese los grados a rotar: " )
    rotated = imutils.rotate_bound( img, int(x) )
    show(rotated, "Imagen rotada")


#Función para cortar la imagen solicitando los valores
def cutImage():
    x1 = input( "Ingrese el valor de x1: " )
    y1 = input( "Ingrese el valor de y1: " )
    x2 = input( "Ingrese el valor de x2: " )
    y2 = input( "Ingrese el valor de y2: " )
    imgCut = img[int( y1 ): int(y2), int( x1 ): int(x2)]
    show(imgCut, "Imagen cortada")


#Función para el menú de transformar
def change():
    global bandera
    bandera = True
    print("1. RGB de un pixel")
    print("2. Recortar imagen" )
    print( "3. Ajustar tamaño de la imagen" )
    print( "4. Redimensionar imagen" )
    print( "5. Rotar imagen" )
    print("6. Volver")
    print("7. Salir")
    opcion = input("Elija una opcion: ")
    if (opcion=="1"):
        rgbImage()
    elif(opcion == "2"):
        cutImage()
    elif(opcion == "3"):
        resize()
    elif (opcion == "4"):
        resize2()
    elif(opcion == "5"):
        rotate()
    elif(opcion == "6"):
        main()
    elif (opcion == "7"):
        sys.exit(0)
    else:
        change()

def Blur():
    Blur = cv2.GaussianBlur( img, (11, 11), 0 )
    show(Blur, "Blur")


#Función para aplicar filtros, se mantuvieron los filtros ya realizados en la tarea moral anterior

def filters():
    global bandera
    bandera = False
    print("1. Blur con kernel gaussiano")
    print("2. Blurring")
    print( "3. Gausiano" )
    print( "4. Median" )
    print("5. Volver")
    print("6. Salir")
    opcion = input( "Elija una opcion: " )
    if (opcion == "1"):
        Blur()

    elif (opcion == "2"):
        Blurring()

    elif(opcion=="3"):
        Gaussian()

    elif (opcion == "4"):
        Median()

    elif(opcion == "5"):
        main()

    elif(opcion == "6"):
        sys.exit( 0 )

    else:
        filters()

def Blurring():
    blur = cv2.blur( img, (5, 5) )
    show( blur, "Blur" )


def Gaussian():

    blur = cv2.GaussianBlur( img, (5, 5), 0 )
    show( blur, "Gaussian" )


def Median():
    median = cv2.medianBlur( img, 5 )
    show( median, "Median")


#Función generica para mostrar la imagen original y la resultante
def show(filter, Name):

    plt.subplot( 121 ), plt.imshow( img ), plt.title( 'Original' )
    plt.xticks( [] ), plt.yticks( [] )
    plt.subplot( 122 ), plt.imshow( filter ), plt.title( Name )
    plt.xticks( [] ), plt.yticks( [] )
    plt.show()
    again()

main()
