import cv2
import numpy as np
import matplotlib.pyplot as plt
import pathlib
import sys

img = cv2.imread( "./mandarina2.jpg") #Es posible utilizar las demás imagenes solo con cambiar la dirección
filename = pathlib.Path().absolute()

def equalizacion():
    gray_image = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    eq = cv2.equalizeHist(gray_image)
    plt.imshow(eq, cmap='gray')
    plt.show()
    main()

def main():
    print("1. Ejericio 3.1 -> Multiplicar cada color ")
    print("2. Equalización")
    print("3. Salir")
    option = input("Elija una opción: ")
    if (option == "1"):
        B = input("Ingrese el Valor B: ")
        G = input( "Ingrese el Valor G: " )
        R = input( "Ingrese el Valor R: " )
        #cv2.imshow( "Original", hist )
        colorBalance(B,G,R, img, True)
        gammaImage = gamma(img)
        result = colorBalance(B, G, R, gammaImage, False)
        result2 = colorBalance( B, G, R, img, False)
        gammaImage2 = gamma(result2)
        show( result, gammaImage2)

    elif(option == "2"):
        equalizacion()

    elif (option == "3"):
        sys.exit()
    else:
        main()


def colorBalance(B, G, R, image, show):
    bImage, gImage, rImage = cv2.split( image )
    b = bImage*int(B);
    g = gImage*int(G)
    r = rImage*int(R)
    newImage = cv2.merge((b,g,r))
    file = str(filename) + "\\balance.png"
    cv2.imwrite(file, newImage)
    img2 = cv2.imread( file)
    if(show):
        cv2.imshow("Multiplicando cada color", img2)

    return img2

def gamma(result):
    invGamma = 1.0 / 1.5
    table = np.array( [((i / 255.0) ** invGamma) * 255
                       for i in np.arange( 0, 256 )] ).astype( "uint8" )
    # apply gamma correction using the lookup table
    imagen = cv2.LUT( result, table )
    return imagen

def show(filter, image):

    plt.subplot( 121 ), plt.imshow( filter ), plt.title( 'Gamma Antes' )
    plt.xticks( [] ), plt.yticks( [] )
    plt.subplot( 122 ), plt.imshow( image ), plt.title( 'Gamma Después' )
    plt.xticks( [] ), plt.yticks( [] )
    plt.show()
    main()

main()
