import cv2
import numpy as np
def inicio():
    choose=input("Laboratorio #1\n1)Cambio de fondo\n2)Contraste y luminosidad\n3)Resize iterativo\n4)Salir\nEjercicios 1 y 4 se encuentran en el archivo tarea.py\nOpción: ")
    if choose=="1":
        x = 0
        while(x!=1 and x!=2):
            x=int(input("Escoge color:\n1)Azul\n2)Verde\nOpción:"))
        if(x == 1):
            x = (255,0,0)
        else:
            x = (0,128,0)
        chageBackground(x)
    if choose == "2":
        alpha = 0
        beta = -1
        while(alpha<1.0 or alpha>3.0):
            alpha = float(input('Contraste [1.0-3.0]: '))
        while(beta<0 or beta>100):
            beta = int(input('luminosidad [0-100]: '))
        contrastNSolorization(alpha, beta)
    if choose == "3":
        x=int(input("Numero de iteraciones:"))
        resize(x)
    if choose == "4":
        exit()
    else:
        print("Error de opción")
        inicio()

def resize(iterations):
    img = cv2.imread('imgs/hb.jpg',
                     cv2.COLOR_BGR2RGB)
    images =[]
    images.append(img)
    x = img.shape
    while(iterations > 0):
        iterations = iterations -1
        images.append(cv2.resize(images[-1], (int(x[1]*0.9), int(x[0]*0.9))))
        images.append(cv2.resize(images[-1], (int(x[1]*1.1), int(x[0]*1.1))))
    images.append(cv2.resize(images[-1], (x[1], x[0])))
    for im in range(len(images)):
        cv2.imshow('image', images[im])
        cv2.waitKey(5000)
    cv2.destroyAllWindows()

    inicio()

def chageBackground(x):
    img = cv2.imread('imgs/hb.jpg',
                     cv2.COLOR_BGR2RGB)
    lower_white = np.array([220, 220, 220], dtype=np.uint8)
    upper_white = np.array([255, 255, 255], dtype=np.uint8)
    mask = cv2.inRange(img, lower_white, upper_white)  # could also use threshold
    mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3)))  # "erase" the small white points in the resulting mask
    mask = cv2.bitwise_not(mask)  # invert mask

    # load background (could be an image too)
    bk = np.full(img.shape, x, dtype=np.uint8)

    # get masked foreground
    fg_masked = cv2.bitwise_and(img, img, mask=mask)

    # get masked background, mask must be inverted
    mask = cv2.bitwise_not(mask)
    bk_masked = cv2.bitwise_and(bk, bk, mask=mask)

    # combine masked foreground and masked background
    final = cv2.bitwise_or(fg_masked, bk_masked)
    mask = cv2.bitwise_not(mask)  # revert mask to original
    cv2.imshow('image', final)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    inicio()

def contrastNSolorization(alpha, beta):
    img = cv2.imread('imgs/hb.jpg',
                     cv2.COLOR_BGR2RGB)
    # new_image = cv.convertScaleAbs(image, alpha=alpha, beta=beta)  through OpencCV
    for y in range(img.shape[0]):
        for x in range(img.shape[1]):
            for c in range(img.shape[2]):
                img[y,x,c] = np.clip(alpha*img[y,x,c] + beta, 0, 255)
    cv2.imshow('image', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    inicio()
inicio()