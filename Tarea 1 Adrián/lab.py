import cv2
def inicio():
    img = cv2.imread('imgs/1.jpg')
    choose=input("Laboratorio #1\n1)RGB\n2)Región\n3)Resize sin aspecto\n4)Resize con aspecto\n5)Rotar\n6)Desenfoque\n7)Salir\nOpción: ")
    if choose=="1":
        x=int(input("X:"))
        y=int(input("Y:"))
        b, g, r= (img[y, x])
        print("Red:"+str(r)+" Green:"+str(g)+" Blue:"+str(b))
        inicio()
    if choose=="2":
        x1 = int(input("X1:"))
        y1 = int(input("Y1:"))
        x2 = int(input("X2:"))
        y2 = int(input("Y2:"))
        roi = img[y1:y2, x1:x2]
        cv2.imshow('image', img)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        cv2.imshow('image', roi)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        inicio()
    if choose == "3":
        w = int(input("\nWidth:"))
        h = int(input("Height:"))
        resized = cv2.resize(img, (w, h))
        cv2.imshow('image', resized)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        inicio()
    if choose == "4":
        w = int(input("\nWidth:"))
        h = int(input("Height:"))
        r = 300.0 / w
        dim = (300, int(h * r))
        resized = cv2.resize(img, dim)
        cv2.imshow('image', resized)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        inicio()
    if choose == "5":
        grados = int(input("\nGrados:"))
        rows, cols = img.shape[:2]
        M = cv2.getRotationMatrix2D((cols / 2, rows / 2), -grados, 1)
        rotate = cv2.warpAffine(img, M, (cols, rows))
        cv2.imshow('image', rotate)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        inicio()
    if choose == "6":
        gauss = cv2.GaussianBlur(img, (9, 9), 0)
        cv2.imshow('image', gauss)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        inicio()
    if choose == "7":
        exit()
    else:
        print("Error de opción")
        inicio()
inicio()