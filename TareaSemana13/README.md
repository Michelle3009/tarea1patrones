# Tarea: Semana 13
Michelle Molina, Miguel Rivas
___

## Problema 1: Clasificador de frutas
El código fuente se encuentra en el directorio `/frutas`

### ¿Cómo probar?

Se deben instalar las dependencias con "npm install".
Se puede probar abriendo el archivo index.html en el navegador. Al abrir el archivo se le mostrará una página dónde debe seleccionar una imagen y esperar unos segundos hasta que se le despliegue la información correspondiente.
 
## Problema 2: Flappy Bird con reconocimiento de voz
El código fuente se encuentra en el directorio `/flappy-bird`.

### ¿Cómo probar?
Se recomienda utilizar `http-server` para *correr* el proyecto.
Si no cuenta con esta herramienta, puede obtenerla con `npm i -g http-server`

* Abra una terminal y colóquese sobre el directorio `/flappy-bird`, ejecute `http-server`.

* Abra el navegador e ingrese al sitio que indica la terminal.

* Conceda permisos de grabación de audio.

* Para verificar el reconocimiento de voz, deberá decir la palabra **up** para modificar la altura del pájaro.